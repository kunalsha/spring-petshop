/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.metpetshop.service.impl;

import com.metpetshop.model.Orders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.metpetshop.dao.OrdersDao;
import com.metpetshop.service.OrdersService;

/**
 *
 * @author Micic
 */
@Service
public class OrderServiceImpl implements OrdersService{

    @Autowired
    private OrdersDao orderDao;
    
    @Override
    public Orders addOrders(Orders orders) {
        return orderDao.addOrders(orders);
    }
    
}
