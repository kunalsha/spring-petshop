<%-- 
    Document   : thanks
    Created on : Jan 13, 2017, 8:37:01 PM
    Author     : Micic
--%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ include file="/WEB-INF/jsp/partials/header.jsp" %>

<div class="container-wrapper">
    <div class="container">
        <section>
            <div class="jumbotron">
                <div class="container">
                    <h1>Thank you for buying!</h1>
                    <h2><a href="${pageContext.request.contextPath}/product/productList/all">Click</a> here to browse more products</h2>
                </div>
            </div>
        </section>


        <%@ include file="/WEB-INF/jsp/partials/footer.jsp" %>
