<%-- 
    Document   : customerManagement
    Created on : Jan 10, 2017, 1:15:48 AM
    Author     : Filip
--%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ include file="/WEB-INF/jsp/partials/header.jsp" %>

<script>
    $(document).ready(function () {
        $('#example').DataTable({
             scrollY: '50vh',
             scrollCollapse: true,
             paging: false
        });
    });
</script>

<div class="container-wrapper">
    <div class="container">
        <div class="page-header">
            <h1>Customer Management Page</h1>

            <p class="lead">This is the customer management page!</p>
            <a href="<spring:url value="/admin" />" class="btn btn-default">Back to Admin Panel</a>
        </div>

        <table id="example" class="display" cellspacing="0" width="100%">
            <thead> 
                <tr class="bg-info">
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Username</th>
                    <th>Enabled</th>
                </tr>
            </thead>
            <c:forEach items="${customerList}" var="customer">
                <tr>
                    <td>${customer.customerName}</td>
                    <td>${customer.customerEmail}</td>
                    <td>${customer.customerPhone}</td>
                    <td>${customer.username}</td>
                    <td>${customer.enabled}</td>
                </tr>
            </c:forEach>
        </table>

        <%@ include file="/WEB-INF/jsp/partials/footer.jsp" %>