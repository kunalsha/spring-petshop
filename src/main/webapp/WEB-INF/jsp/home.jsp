<%-- 
    Document   : home
    Created on : Jan 3, 2017, 2:07:50 AM
    Author     : Filip
--%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ include file="/WEB-INF/jsp/partials/header.jsp" %>

<!-- Carousel
================================================== -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <img class="first-slide home-image" src="<c:url value="/resources/images/back1.jpg" />" alt="First slide">
            <div class="container">
                <div class="carousel-caption">
                    <h1>Welcome to Met PetShop </h1>
                    <p>Here you can browse and buy products for your pet. Food and grooming supplies avaliable. Order Now!</p>
                </div>
            </div>
        </div>

        <div class="item">
            <img class="second-slide home-image" src="<c:url value="/resources/images/back2.jpg" />" alt="Second slide">
            <div class="container">
                <div class="carousel-caption">
                    <h1>Welcome to Met PetShop </h1>
                    <p>Here you can browse and buy products for your pet. Food and grooming supplies avaliable. Order Now!</p>
                </div>
            </div>
        </div>
        <div class="item">
            <img class="third-slide home-image " src="<c:url value="/resources/images/back3.jpg" />" alt="Third slide">
            <div class="container">
                <div class="carousel-caption">
                    <h1>Welcome to Met PetShop </h1>
                    <p>Here you can browse and buy products for your pet. Food and grooming supplies avaliable. Order Now!</p>
                </div>
            </div>
        </div>
    </div>
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div><!-- /.carousel -->


<div class="container marketing">

    <!-- Three columns of text below the carousel -->
    <div class="row">
        <div class="col-lg-4">
            <img class="img-circle" src="<c:url value="/resources/images/front1.jpg"/>" alt="Dog Food Image" width="140" height="140">
            <h2>Dog Food</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed urna nunc, mollis vitae pretium vel, tincidunt at erat.</p>
        </div>
        <div class="col-lg-4">
            <img class="img-circle" src="<c:url value="/resources/images/front2.jpg"/>" alt="Grooming Products Image" width="140" height="140">
            <h2>Grooming Products</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed urna nunc, mollis vitae pretium vel, tincidunt at erat.</p>
        </div>
        <div class="col-lg-4">
            <img class="img-circle" src="<c:url value="/resources/images/front3.jpeg"/>" alt="Cat Food Image" width="140" height="140">
            <h2>Cat Food</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed urna nunc, mollis vitae pretium vel, tincidunt at erat.</p>
        </div>
    </div>


    <%@ include file="/WEB-INF/jsp/partials/footer.jsp" %>